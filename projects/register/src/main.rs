use std::collections::HashMap;
use std::io;

fn main() {
    let options: Vec<&str> = ["Add", "Remove", "List"].to_vec();
    let department: Vec<&str> = ["Sales", "IT", "Engineering"].to_vec();

    let option = input(options);
    println!("{}", option);
}

fn input(vec: Vec<&str>) -> String {

    let mut format = String::new();
    for i in vec.iter() {
        let mut i = i.to_string();
        i.push_str(" | ");
        format.push_str(&i);
    }

    let mut input = String::new();
    println!("{}", format);
    io::stdin()
        .read_line(&mut input)
        .expect("could not read input");

    input
}

fn process(input: &str, options: Vec<&str>) {
    enum Set {
        Add,
        Remove,
    }

    enum Department {
        Engineering,
        Sales,
        IT,
        Finance,
        Nothing,
    }

    match input.to_lowercase().as_str() {
        "add" => println!("add"),
        "remove" => println!("remove"),
        "list" => println!("List"),
        _ => (),
    };
}

fn process_set(input: String) {
    match input.to_lowercase().as_str() {
        "add" => println!("add"),
        "remove" => println!("remove"),
        "list" => println!("List"),
        _ => (),
    };
}

fn process_department(input: String) {
    match input.to_lowercase().as_str() {
        "sales" => println!("sales"),
        "it" => println!("it"),
        "finance" => println!("finance"),
        _ => (),
    };
}
