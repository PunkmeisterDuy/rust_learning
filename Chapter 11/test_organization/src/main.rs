fn main() {
    println!("Hello, world!");
}


/*
#[cfg(test)] // doesnt run during cargo build only cargo test
cfg stands for configuration: the config is test+

unit testing tests individual units or parts of your code
integration testing tests integrating code with other parts of your library

cargo test --test integration_test ; runs integration tests

*/